﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionController : ControllerBase
    {
        private readonly IPromotionService _promotionService;

        public PromotionController(IPromotionService promotionService)
        {
            this._promotionService = promotionService;
        }

        //get request which returns all promotions and their submissions.
        //api/promotion
        [HttpGet]
        public async Task<IEnumerable<Promotion>> Get()
        {
            return await _promotionService.GetAllAsync();
        }

        //get request which returns promotion and its submissions based on the promotion ID
        // GET api/promotion/5
        [HttpGet("{id}")]
        public async Task<IEnumerable<Promotion>> Get(Guid id)
        {
            return await _promotionService.FilterPromotionsByIdAsync(id);
        }

        //Post request which takes json Promotion from the request body and saves the promotion
        //api/promoion/5 -> for update of promotion with ID 5
        //api/promotion -> for creating promotion
        [HttpPost]
        public void Post([FromBody]Promotion val)
        {
             _promotionService.SavePromotion(val);
        }

        //delete request which takes the ID of the promotion and flags it as deleted
        //api/promotion/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _promotionService.DeletePromotion(id);
        }
    }
}