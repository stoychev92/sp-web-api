﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubmissionController : ControllerBase
    {
        private readonly ISubmissionService _submissionService;

        public SubmissionController(ISubmissionService submissionService)
        {
            this._submissionService = submissionService;
        }

        //returns all submissions.
        //api/submission
        [HttpGet]
        public async Task<IEnumerable<Submission>> Get()
        {
            return await _submissionService.GetAllAsync();
        }

        //get request which returns submission based on its id 
        // GET api/submission/5
        [HttpGet("{id}")]
        public async Task<IEnumerable<Submission>> Get(Guid id)
        {
            return await _submissionService.FilterSubmissionsByIdAsync(id);
        }

        //Post request which takes json submission from the request body and saves it
        [HttpPost]
        public void Post([FromBody]Submission val)
        {
            _submissionService.SaveSubmission(val);
        }

        //delete request which takes the ID of the submission and flags it as deleted
        //api/submission/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _submissionService.DeleteSubmission(id);
        }
    }
}