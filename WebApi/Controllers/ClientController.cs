﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            this._clientService = clientService;
        }

        //Get request for API which returns all Clients
        //api/client
        [HttpGet]
        public async Task<IEnumerable<Client>> Get()
        {
            return await _clientService.GetAllAsync();
        }

        //get request which returns client based on its id with its promotions and submissions included
        // GET api/client/5
        [HttpGet("{id}")]
        public async Task<IEnumerable<Client>> Get(Guid id)
        {
            return await _clientService.FilterClientsByIdAsync(id);
        }

        //Post request which takes json Client from the request body and saves the client
        [HttpPost]
        public void Post([FromBody]Client val)
        {
            _clientService.SaveClient(val);
        }

        //delete request which takes the ID of the client and flags it as deleted
        //api/client/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _clientService.DeleteClient(id);
        }
    }
}