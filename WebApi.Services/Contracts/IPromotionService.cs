﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models.Models;

namespace WebApi.Services.Contracts
{
    public interface IPromotionService
    {
        Task<IEnumerable<Promotion>> GetAllAsync();

        Task<IEnumerable<Promotion>> FilterPromotionsByIdAsync(Guid promotionId);

        Task<IEnumerable<Promotion>> FilterPromotionsByNameAsync(string name);

        void DeletePromotion(Guid promotionId);

        void SavePromotion(Promotion updatedPromotion);

    }
}
