﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models.Models;

namespace WebApi.Services.Contracts
{
    public interface ISubmissionService
    {
        Task<IEnumerable<Submission>> GetAllAsync();

        Task<IEnumerable<Submission>> FilterSubmissionsByIdAsync(Guid submissionId);

        Task<IEnumerable<Submission>> FilterSubmissionsByFirstNameAsync(string firstName);

        Task<IEnumerable<Submission>> FilterSubmissionsByLastNameAsync(string lastName);

        void DeleteSubmission(Guid submissionId);

        void SaveSubmission(Submission submission);
    }
}
