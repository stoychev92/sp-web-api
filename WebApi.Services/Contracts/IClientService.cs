﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models.Models;

namespace WebApi.Services.Contracts
{
    public interface IClientService
    {
        Task<IEnumerable<Client>> GetAllAsync();

        Task<IEnumerable<Client>> FilterClientsByIdAsync(Guid clientId);

        Task<IEnumerable<Client>> FilterClientsByNameAsync(string name);

        void DeleteClient(Guid clientId);

        void SaveClient(Client client);
    }
}
