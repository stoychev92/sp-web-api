﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Data.Data;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Services.Services
{
    public class PromotionService : IPromotionService
    {
        private readonly WebApiContext context;

        public PromotionService(WebApiContext context)
        {
            this.context = context;
        }

        //this method returns all promotions with their submissions as a json
        public async Task<IEnumerable<Promotion>> GetAllAsync()
        {
            var query = this.context.Promotions.Include(x => x.Client).Include(y => y.Submissions);
            string output = JsonConvert.SerializeObject(query);
            return await query.ToListAsync();
        }

        //this method returns all promotions with their submissions based on the promotion ID
        public async Task<IEnumerable<Promotion>> FilterPromotionsByIdAsync(Guid promotionId)
        {
            var query = this.context.Promotions.Where(x => x.Id == promotionId).Include(y => y.Submissions);

            return await query.ToListAsync();
        }

        //this method returns all promotions with their submissions based on the promotion name. Not implemented yet in the controllers
        public async Task<IEnumerable<Promotion>> FilterPromotionsByNameAsync(string name)
        {
            var query = this.context.Promotions.Where(x => x.Name.ToLower().Contains(name.ToLower())).Include(y => y.Submissions);

            return await query.ToListAsync();
        }

        //this method deletes the promotion based on the ID. Deletion is handled by a boolean flag isDeleted. DateDeleted is also updated.
        public void DeletePromotion(Guid promotionId)
        {

            if (promotionId == null)
            {
                return;
            }
            else
            {
                Promotion updatedPromotion = this.context.Promotions.Where(x => x.Id == promotionId).FirstOrDefault();
                updatedPromotion.IsDeleted = true;
                updatedPromotion.DeletedOn = DateTime.Now;
                this.context.Update(updatedPromotion);
            }

            this.context.SaveChanges();
        }

        //this method saves the promotions. If a promotion does not contain Id value, it is marked as new promotion and is being created.
        //If Id is included, the promotion is being updated
        public void SavePromotion(Promotion updatedPromotion)
        {
            if (updatedPromotion.Id == null)
            {
                updatedPromotion.Id = new Guid();
                this.context.Add(updatedPromotion);
            }
            else
            {
                var promotion = this.context.Promotions.SingleOrDefault(c => c.Id == updatedPromotion.Id);
                if (promotion == null)
                {
                    this.context.Add(updatedPromotion);
                }
                else
                {
                    if (promotion.Name != updatedPromotion.Name)
                    {
                        promotion.Name = updatedPromotion.Name;
                    }
                    if (promotion.ClientId != updatedPromotion.ClientId)
                    {
                        promotion.ClientId = updatedPromotion.ClientId;
                    }
                }
            }

            this.context.SaveChanges();
        }
    }
}
