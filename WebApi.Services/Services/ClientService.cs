﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Data.Data;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Services.Services
{
    public class ClientService : IClientService
    {
        private readonly WebApiContext context;

        public ClientService(WebApiContext context)
        {
            this.context = context;
        }

        //this method returns all clients as a json
        public async Task<IEnumerable<Client>> GetAllAsync()
        {
            var query = this.context.Clients.Include(x => x.Promotions).ThenInclude(y => y.Submissions);
            string output = JsonConvert.SerializeObject(query);
            return await query.ToListAsync();
        }

        //this method returns 1 client based on the ID with their promotions and promotion submissions
        public async Task<IEnumerable<Client>> FilterClientsByIdAsync(Guid clientId)
        {
            var query = this.context.Clients.Where(x => x.Id == clientId).Include(x => x.Promotions).ThenInclude(y => y.Submissions);

            return await query.ToListAsync();
        }

        //this method filters clients by name. Not implemented yet in the controller
        public async Task<IEnumerable<Client>> FilterClientsByNameAsync(string name)
        {
            var query = this.context.Clients.Where(x => x.Name.ToLower().Contains(name.ToLower())).Include(x => x.Promotions).ThenInclude(y => y.Submissions);

            return await query.ToListAsync();
        }

        //this method deletes a client based on the client ID. The deletion is being handled with boolean field isDeleted and DeletedOn (date)
        public void DeleteClient(Guid clientId)
        {

            if (clientId == null)
            {
                return;
            }
            else
            {
                Client updatedClient = this.context.Clients.Where(x => x.Id == clientId).FirstOrDefault();
                updatedClient.IsDeleted = true;
                updatedClient.DeletedOn = DateTime.Now;
                this.context.Update(updatedClient);
            }

            this.context.SaveChanges();
        }


        //this method saves the client. If the Client ID is not present, method creates a new client. Otherwise client is being updated
        public void SaveClient(Client updatedClient)
        {
            if (updatedClient.Id == null)
            {
                updatedClient.Id = new Guid();
                this.context.Add(updatedClient);
            }
            else
            {
                var client = this.context.Clients.SingleOrDefault(c => c.Id == updatedClient.Id);
                if (client == null)
                {
                    this.context.Add(updatedClient);
                }
                else if (client.Name != updatedClient.Name)
                {
                    client.Name = updatedClient.Name;
                }
            }

            this.context.SaveChanges();
        }
    }
}
