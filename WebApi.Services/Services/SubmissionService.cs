﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Data.Data;
using WebApi.Models.Models;
using WebApi.Services.Contracts;

namespace WebApi.Services.Services
{
    public class SubmissionService : ISubmissionService
    {
        private readonly WebApiContext context;

        public SubmissionService(WebApiContext context)
        {
            this.context = context;
        }

        //this method returns all submissions as a json
        public async Task<IEnumerable<Submission>> GetAllAsync()
        {
            var query = this.context.Submissions;
            string output = JsonConvert.SerializeObject(query);
            return await query.ToListAsync();
        }

        //this method returns submissions based on their ID.
        public async Task<IEnumerable<Submission>> FilterSubmissionsByIdAsync(Guid submissionId)
        {
            var query = this.context.Submissions.Where(x => x.Id == submissionId);

            return await query.ToListAsync();
        }

        //this method returs submissions based on firstname. Not implemented yet in the controllers
        public async Task<IEnumerable<Submission>> FilterSubmissionsByFirstNameAsync(string firstName)
        {
            var query = this.context.Submissions.Where(x => x.FirstName.ToLower().Contains(firstName.ToLower()));

            return await query.ToListAsync();
        }

        //this method returs submissions based on last name. Not implemented yet in the controllers
        public async Task<IEnumerable<Submission>> FilterSubmissionsByLastNameAsync(string lastName)
        {
            var query = this.context.Submissions.Where(x => x.LastName.ToLower().Contains(lastName.ToLower()));

            return await query.ToListAsync();
        }

        //this method saves the submission. It is used both for updating submissions (if ID is present) or creating new submissions(when ID is missing)
        public void SaveSubmission(Submission updatedSubmission)
        {
            if (updatedSubmission.Id == null)
            {
                updatedSubmission.Id = new Guid();
                this.context.Add(updatedSubmission);
            }
            else
            {
                var submission = this.context.Submissions.SingleOrDefault(c => c.Id == updatedSubmission.Id);
                if (submission == null)
                {
                    this.context.Add(updatedSubmission);
                }
                else
                {
                    if (submission.FirstName != updatedSubmission.FirstName)
                    {
                        submission.FirstName = updatedSubmission.FirstName;
                    }
                    if (submission.LastName != updatedSubmission.LastName)
                    {
                        submission.LastName = updatedSubmission.LastName;
                    }
                    if (submission.Address != updatedSubmission.Address)
                    {
                        submission.Address = updatedSubmission.Address;
                    }
                    if (submission.PromotionId != updatedSubmission.PromotionId)
                    {
                        submission.PromotionId = updatedSubmission.PromotionId;
                    }
                }
            }

            this.context.SaveChanges();
        }

        //this method is used to delete submissions based on submission ID
        public void DeleteSubmission(Guid submissionId)
        {

            if (submissionId == null)
            {
                return;
            }
            else
            {
                Submission updatedSubmission = this.context.Submissions.Where(x => x.Id == submissionId).FirstOrDefault();
                updatedSubmission.IsDeleted = true;
                updatedSubmission.DeletedOn = DateTime.Now;
                this.context.Update(updatedSubmission);
            }

            this.context.SaveChanges();
        }
    }
}
