﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Abstract
{
    public class Entity : IAuditable, IDeletable
    {
        [Key]
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DeletedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? CreatedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? ModifiedOn { get; set; }
    }
}
