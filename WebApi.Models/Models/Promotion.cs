﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WebApi.Models.Abstract;

namespace WebApi.Models.Models
{
    public class Promotion : Entity
    {
        public string Name { get; set; }

        public Guid ClientId { get; set; }

        [JsonIgnore]
        public Client Client { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public IEnumerable<Submission> Submissions { get; set; }
    }
}
