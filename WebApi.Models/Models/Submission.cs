﻿using Newtonsoft.Json;
using System;
using WebApi.Models.Abstract;

namespace WebApi.Models.Models
{
    public class Submission : Entity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public Guid PromotionId { get; set; }

        [JsonIgnore]
        public Promotion Promotion { get; set; }
    }
}
