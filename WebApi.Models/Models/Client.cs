﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WebApi.Models.Abstract;

namespace WebApi.Models.Models
{
    public class Client : Entity
    {
        public string Name { get; set; }

        public IEnumerable<Promotion> Promotions { get; set; }
    }
}
