﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Data.Migrations
{
    public partial class updated_submission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions");

            migrationBuilder.AlterColumn<Guid>(
                name: "PromotionId",
                table: "Submissions",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions",
                column: "PromotionId",
                principalTable: "Promotions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions");

            migrationBuilder.AlterColumn<Guid>(
                name: "PromotionId",
                table: "Submissions",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions",
                column: "PromotionId",
                principalTable: "Promotions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
