﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Data.Migrations
{
    public partial class reverted_changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotions_Clients_ClientId1",
                table: "Promotions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Promotions_PromotionId1",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_PromotionId1",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Promotions_ClientId1",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "PromotionId1",
                table: "Submissions");

            migrationBuilder.DropColumn(
                name: "ClientId1",
                table: "Promotions");

            migrationBuilder.AlterColumn<Guid>(
                name: "PromotionId",
                table: "Submissions",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ClientId",
                table: "Promotions",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_PromotionId",
                table: "Submissions",
                column: "PromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_Promotions_ClientId",
                table: "Promotions",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotions_Clients_ClientId",
                table: "Promotions",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions",
                column: "PromotionId",
                principalTable: "Promotions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotions_Clients_ClientId",
                table: "Promotions");

            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Promotions_PromotionId",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_PromotionId",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Promotions_ClientId",
                table: "Promotions");

            migrationBuilder.AlterColumn<string>(
                name: "PromotionId",
                table: "Submissions",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "PromotionId1",
                table: "Submissions",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ClientId",
                table: "Promotions",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "ClientId1",
                table: "Promotions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_PromotionId1",
                table: "Submissions",
                column: "PromotionId1");

            migrationBuilder.CreateIndex(
                name: "IX_Promotions_ClientId1",
                table: "Promotions",
                column: "ClientId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotions_Clients_ClientId1",
                table: "Promotions",
                column: "ClientId1",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Promotions_PromotionId1",
                table: "Submissions",
                column: "PromotionId1",
                principalTable: "Promotions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
